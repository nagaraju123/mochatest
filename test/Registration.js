const assert = require('chai').assert;
const addnumbers = require('../app').addnumbers;
const login = require('../app').login;
const funct = require('../app').funct;
const funct1 = require('../app').funct1;
const Register=require('../app').Register;
const passwordvalidation=require('../app').passwordvalidation;
describe('App', function () {

      
  it('validate valid username', function () {
    assert.equal(Register('te test','Test@1234'), 'username contains spaces');
  });

it('validate registarion  username', function () {
  assert.equal(Register('tes','T123'), 'password must contains special chars and minimum 8 digits');
});

it('validate registarion  lowecase pwd', function () {
  assert.equal(Register('test','TESTING'), 'password must contains lowercase and special chars and minimum 8 digits');
});

it('validate registarion upper case pwd', function () {
  assert.equal(Register('test','ert'), 'password must contains uppercase and special chars and minimum 8 digits');
});

it('validate registarion special chars pwd', function () {
  assert.equal(Register('test','Tertf3rt'), 'password must contains special chars and minimum 8 digits');
});

it('validate registarion valid user and pwd', function () {
  assert.equal(Register('test','Test@1234'), 'Registration completed successfully');
});
})



