const assert = require('chai').assert;
const addnumbers = require('../app').addnumbers;
const login = require('../app').login;
const funct = require('../app').funct;
const funct1 = require('../app').funct1;
const Register=require('../app').Register;
const passwordvalidation=require('../app').passwordvalidation;
describe('App', function () {

  
  it('validate with invalid username', function () {
    assert.equal(login('1test', 'Test@123'), 'invalid username ');
  });

  it('validate with invalid pwd', function () {
    assert.equal(login('test', 'Test@1234'), 'invalid password');
  });

  it('validate with invalid user and invalid pwd', function () {
    assert.equal(login('testr', 'ert'), 'invallid username / password');
  });
  it('validate single param', function () {
    assert.equal(login('test'), 'please enter username /password');
  });
  it('validate with valid credentials', function () {
    assert.equal(login('test','Test@123'), 'valid credentials and Login successfully');
  });
})
