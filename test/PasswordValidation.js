const assert = require('chai').assert;
const addnumbers = require('../app').addnumbers;
const login = require('../app').login;
const funct = require('../app').funct;
const funct1 = require('../app').funct1;
const Register=require('../app').Register;
const passwordvalidation=require('../app').passwordvalidation;
describe('App', function () {
    it('validate username', function () {
      assert.equal(passwordvalidation('test', 'T123'), 'password must contains special chars and minimum 8 digits');
    });
  
    it('validate lowecase pwd', function () {
      assert.equal(passwordvalidation('test', 'TESTING'), 'password must contains lowercase and special chars and minimum 8 digits');
    });
  
    it('validate upper case pwd', function () {
      assert.equal(passwordvalidation('test', 'ert'), 'password must contains uppercase and special chars and minimum 8 digits');
    });
  
    it('validate special chars pwd', function () {
      assert.equal(passwordvalidation('test', 'Tertf3rt'), 'password must contains special chars and minimum 8 digits');
    });
  
    it('validate valid user and pwd', function () {
      assert.equal(passwordvalidation('test', 'Test@1234'), 'Login successfully');
    });
  })


  



