module.exports = {
    Register: function (user, pwd) {
        if (user && pwd) {
            
                if (typeof user == 'string' && user.match(" ")) return "username contains spaces"
    
                var condition = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,}$/
                var condition1 = /^[A-Z_\-]+$/
                var condition2 = /^[a-z_\-]+$/
                var condition3 = /^(?=.*[ -\/:-@\[-\`{-~])+$/
    
    
                if (pwd.match(condition)) return 'Registration completed successfully'
                else if (pwd.match(condition1)) return 'password must contains lowercase and special chars and minimum 8 digits'
                else if (pwd.match(condition2)) return 'password must contains uppercase and special chars and minimum 8 digits'
                else if (!pwd.match(condition3)) return 'password must contains special chars and minimum 8 digits'
    
            } else return 'invalid user'
        }
    }
    